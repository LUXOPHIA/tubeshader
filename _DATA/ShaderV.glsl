﻿#version 430

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%【ルーチン】

float CatmullRom( float P0_, float P1_, float P2_, float P3_, float T_ )
{
  return ( ( ( -0.5 * P0_ + 1.5 * P1_ - 1.5 * P2_ + 0.5 * P3_ ) * T_
             +        P0_ - 2.5 * P1_ + 2.0 * P2_ - 0.5 * P3_ ) * T_
             -  0.5 * P0_             + 0.5 * P2_             ) * T_
                          +       P1_;
}

vec2 CatmullRom( vec2 P0_, vec2 P1_, vec2 P2_, vec2 P3_, float T_ )
{
  vec2 Result;

  Result.x = CatmullRom( P0_.x, P1_.x, P2_.x, P3_.x, T_ );
  Result.y = CatmullRom( P0_.y, P1_.y, P2_.y, P3_.y, T_ );

  return Result;
}

vec3 CatmullRom( vec3 P0_, vec3 P1_, vec3 P2_, vec3 P3_, float T_ )
{
  vec3 Result;

  Result.x = CatmullRom( P0_.x, P1_.x, P2_.x, P3_.x, T_ );
  Result.y = CatmullRom( P0_.y, P1_.y, P2_.y, P3_.y, T_ );
  Result.z = CatmullRom( P0_.z, P1_.z, P2_.z, P3_.z, T_ );

  return Result;
}

vec4 CatmullRom( vec4 P0_, vec4 P1_, vec4 P2_, vec4 P3_, float T_ )
{
  vec4 Result;

  Result.x = CatmullRom( P0_.x, P1_.x, P2_.x, P3_.x, T_ );
  Result.y = CatmullRom( P0_.y, P1_.y, P2_.y, P3_.y, T_ );
  Result.z = CatmullRom( P0_.z, P1_.z, P2_.z, P3_.z, T_ );
  Result.w = CatmullRom( P0_.w, P1_.w, P2_.w, P3_.w, T_ );

  return Result;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%【共通定数】

layout( std140 ) uniform TViewerScal
{
  layout( row_major ) mat4 _ViewerScal;
};

layout( std140 ) uniform TCameraProj
{
  layout( row_major ) mat4 _CameraProj;
};

layout( std140 ) uniform TCameraPose
{
  layout( row_major ) mat4 _CameraPose;
};

layout( std140 ) uniform TShaperPose
{
  layout( row_major ) mat4 _ShaperPose;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%【共通変数】

struct TLing
{
  vec4 Center;
  vec4 Radius;
};

layout( std430 ) buffer TLings
{
  readonly TLing _Lings[];
};

layout( std430 ) buffer TDivZ
{
  readonly uint _DivZ;
};

//############################################################################## ■

in vec4 _SenderPos;
in vec2 _SenderTex;

//------------------------------------------------------------------------------

out TSenderVF
{
  vec4 Pos;
  vec2 Tex;
}
_Result;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

TLing CatmullRom( TLing P0_, TLing P1_, TLing P2_, TLing P3_, float T_ )
{
  TLing Result;

  Result.Center = CatmullRom( P0_.Center, P1_.Center, P2_.Center, P3_.Center, T_ );
  Result.Radius = CatmullRom( P0_.Radius, P1_.Radius, P2_.Radius, P3_.Radius, T_ );

  return Result;
}

TLing Interp( float T_ )
{
  float Tx = floor( T_ );
  int   Ti = int( Tx );
  float Td = T_ - Tx;

  return CatmullRom( _Lings[ Ti+0 ], _Lings[ Ti+1 ], _Lings[ Ti+2 ], _Lings[ Ti+3 ], Td );
}

////////////////////////////////////////////////////////////////////////////////

void main()
{
  TLing L = Interp( _SenderTex.y );

  vec4 P;
  P.x = L.Center.x + L.Radius.x * _SenderPos.x;
  P.y = L.Center.y + L.Radius.y * _SenderPos.y;
  P.z = L.Center.z;
  P.w = _SenderPos.w;

  _Result.Pos = _ShaperPose * P;
  _Result.Tex = _SenderTex;

  gl_Position = _ViewerScal * _CameraProj * inverse( _CameraPose ) * _Result.Pos;
}

//############################################################################## ■
