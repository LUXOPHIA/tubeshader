﻿unit Core;

interface //#################################################################### ■

uses LUX, LUX.D2, LUX.D3, LUX.D4,
     LUX.GPU.OpenGL.Atom.Buffer,
     LUX.GPU.OpenGL.Atom.Buffer.Verter,
     LUX.GPU.OpenGL.Atom.Buffer.Unifor,
     LUX.GPU.OpenGL.Atom.Buffer.StoBuf,
     LUX.GPU.OpenGL.Matery,
     LUX.GPU.OpenGL.Shaper;

type //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【型】

     //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【レコード】

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TTubeLing

     TTubeLing = record
     private
     public
       Center :TSingle4D;
       Radius :TSingle4D;
       /////
       constructor Create( const CX_,CY_,CZ_,RX_,RY_:Single );
     end;

     //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【クラス】

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TGLMateryTube

     TGLMateryTube = class( TGLMatery )
     private
     protected
     public
       constructor Create;
       destructor Destroy; override;
     end;

     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TGLShaperTube

     TGLShaperTube = class( TGLShaperFace )
     private
       ///// メソッド
       procedure InitLings;
       procedure MakeModel;
     protected
       _Count :Integer;
       _Lings :TGLStoBuf<TTubeLing>;
       _DivX  :Integer;
       _DivY  :Integer;
       _DivZ  :TGLStoBuf<Cardinal>;
       ///// アクセス
       procedure SetCount( const Count_:Integer );
       function GetLings( const I_:Integer ) :TTubeLing;
       procedure SetLings( const I_:Integer; const Ling_:TTubeLing );
       function GetDivX :Cardinal;
       procedure SetDivX( const DivX_:Cardinal );
       function GetDivY :Cardinal;
       procedure SetDivY( const DivY_:Cardinal );
       function GetDivZ :Cardinal;
       procedure SetDivZ( const DivZ_:Cardinal );
       ///// メソッド
       procedure BeginDraw; override;
       procedure DrawMain; override;
       procedure EndDraw; override;
     public
       constructor Create; override;
       destructor Destroy; override;
       ///// プロパティ
       property Count                     :Integer   read   _Count write SetCount;
       property Lings[ const I_:Integer ] :TTubeLing read GetLings write SetLings;
       property DivX                      :Cardinal  read GetDivX  write SetDivX ;
       property DivY                      :Cardinal  read GetDivY  write SetDivY ;
       property DivZ                      :Cardinal  read GetDivZ  write SetDivZ ;
     end;

//const //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【定数】

//var //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【変数】

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【ルーチン】

implementation //############################################################### ■

uses System.Math,
     Winapi.OpenGL, Winapi.OpenGLext;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【レコード】

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TTubeLing

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& private

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& public

constructor TTubeLing.Create( const CX_,CY_,CZ_,RX_,RY_:Single );
begin
     Center.X := CX_;
     Center.Y := CY_;
     Center.Z := CZ_;
     Radius.X := RX_;
     Radius.Y := RY_;
end;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【クラス】

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TGLMateryTube

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& private

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& protected

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& public

constructor TGLMateryTube.Create;
begin
     inherited;

     with _Engine do
     begin
          with Verters do
          begin
             //Add( 0{BinP}, '_SenderPos'{Name}, 3{EleN}, GL_FLOAT{EleT} );
               Del( 1{Bind} );
             //Add( 2{BinP}, '_SenderTex'{Name}, 2{EleN}, GL_FLOAT{EleT} );
          end;

          with Unifors do
          begin
             //Add( 0{BinP}, 'TViewerScal'{Name} );
             //Add( 1{BinP}, 'TCameraProj'{Name} );
             //Add( 2{BinP}, 'TCameraPose'{Name} );
             //Add( 3{BinP}, 'TShaperPose'{Name} );
          end;

          with StoBufs do
          begin
               Add( 0{Bind}, 'TLings'{Name} );
               Add( 1{Bind}, 'TDivZ'{Name} );
          end;

          with Imagers do
          begin
               Del( 0{Bind} );
          end;
     end;
end;

destructor TGLMateryTube.Destroy;
begin

     inherited;
end;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TGLShaperTube

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& private

/////////////////////////////////////////////////////////////////////// メソッド

procedure TGLShaperTube.InitLings;
var
   I :Integer;
   L :TTubeLing;
begin
     for I := 0 to _Lings.Count-1 do
     begin
          L.Center.X := RandG( 0, 1 );
          L.Center.Y := RandG( 0, 1 );
          L.Center.Z := I + (Random-0.5)/2 - 1;

          L.Radius.X := Random+Random;
          L.Radius.Y := Random+Random;

          _Lings[ I ] := L;
     end;
end;

procedure TGLShaperTube.MakeModel;
var
   DZ :Integer;
//······································
     function IYXtoJ( const I_,Z_,X_,Y_:Integer ) :Cardinal;
     begin
          Result := ( ( I_ * DZ + Z_ ) * ( _DivY+1 ) + Y_ ) * ( _DivX+1 ) + X_;
     end;
//······································
var
   PsN, EsN, N, Z, Y, X, I :Integer;
   T :TSingle2D;
   R, A :Single;
   P :TSingle3D;
   E :TCardinal3D;
begin
     DZ := DivZ;

     PsN := ( _Count-1 ) * DZ           * ( _DivY+1 ) * ( _DivX+1 );
     EsN := ( _Count-1 ) * DZ * 2{Face} *   _DivY     *   _DivX    ;
     _PosBuf.Count := PsN;
     _TexBuf.Count := PsN;
     _EleBuf.Count := EsN;

     I := 0;
     for N := 0 to _Count-2 do
     begin
          for Z := 1 to DZ do
          begin
               R := Z / DZ;
               for Y := 0 to _DivY do
               begin
                    P.Z := N + Y / _DivY;
                    T.Y := 1 + P.Z;
                    for X := 0 to _DivX do
                    begin
                         T.X := X / _DivX;

                         A := Pi2 * T.X;
                         P.X := R * Cos( A );
                         P.Y := R * Sin( A );

                         _PosBuf[ I ] := P;
                         _TexBuf[ I ] := T;

                         Inc( I );
                    end;
               end;
          end;
     end;

     I := 0;
     for N := _Count-2 downto 0 do
     begin
          for Z := 0 to DZ-1 do
          begin
               for Y := 0 to _DivY-1 do
               begin
                    for X := 0 to _DivX-1 do
                    begin
                         E.X := IYXtoJ( N, Z, X+0, Y+0 );
                         E.Y := IYXtoJ( N, Z, X+1, Y+0 );
                         E.Z := IYXtoJ( N, Z, X+1, Y+1 );

                         _EleBuf[ I ] := E;  Inc( I );

                         E.X := IYXtoJ( N, Z, X+1, Y+1 );
                         E.Y := IYXtoJ( N, Z, X+0, Y+1 );
                         E.Z := IYXtoJ( N, Z, X+0, Y+0 );

                         _EleBuf[ I ] := E;  Inc( I );
                    end;
               end;
          end;
     end;
end;

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& protected

/////////////////////////////////////////////////////////////////////// アクセス

procedure TGLShaperTube.SetCount( const Count_:Integer );
var
   I :Integer;
begin
     _Count := Count_;

     _Lings.Count := _Count + 2;

     InitLings;
     MakeModel;
end;

function TGLShaperTube.GetLings( const I_:Integer ) :TTubeLing;
begin
     Result := _Lings[ I_ ];
end;

procedure TGLShaperTube.SetLings( const I_:Integer; const Ling_:TTubeLing );
begin
     _Lings[ I_ ] := Ling_;
end;

//------------------------------------------------------------------------------

function TGLShaperTube.GetDivX :Cardinal;
begin
     Result := _DivX;
end;

procedure TGLShaperTube.SetDivX( const DivX_:Cardinal );
begin
     _DivX := DivX_;
end;

function TGLShaperTube.GetDivY :Cardinal;
begin
     Result := _DivY;
end;

procedure TGLShaperTube.SetDivY( const DivY_:Cardinal );
begin
     _DivY := DivY_;
end;

function TGLShaperTube.GetDivZ :Cardinal;
begin
     Result := _DivZ[ 0 ];
end;

procedure TGLShaperTube.SetDivZ( const DivZ_:Cardinal );
begin
     _DivZ[ 0 ] := DivZ_;
end;

/////////////////////////////////////////////////////////////////////// メソッド

procedure TGLShaperTube.BeginDraw;
begin
     inherited;

     glBlendFunc( GL_SRC_ALPHA, GL_ONE );

     _Lings.Use( 0{Bind} );
     _DivZ .Use( 1{Bind} );
end;

procedure TGLShaperTube.DrawMain;
begin
     _EleBuf.Draw;
end;

procedure TGLShaperTube.EndDraw;
begin
     _Lings.Unuse( 0{Bind} );
     _DivZ .Unuse( 1{Bind} );

     inherited;
end;

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& public

constructor TGLShaperTube.Create;
begin
     inherited;

     _Lings := TGLStoBuf<TTubeLing>.Create( GL_STATIC_DRAW );
     _DivZ  := TGLStoBuf<Cardinal> .Create( GL_STATIC_DRAW );  _DivZ.Count := 1;

     Matery := TGLMateryTube.Create;

     _DivX    := 32;
     _DivY    := 32;
     _DivZ[0] := 32;

     Count := 100;
end;

destructor TGLShaperTube.Destroy;
begin
     _Lings.DisposeOf;
     _DivZ .DisposeOf;

     inherited;
end;

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$【ルーチン】

//############################################################################## □

initialization //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 初期化

finalization //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 最終化

end. //######################################################################### ■